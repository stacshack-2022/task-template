#!/bin/bash

NAMESPACE=$1

if [ -z "$NAMESPACE" ]
then
      echo "Usage: create-task-repo.sh <namespace>"
      exit 1
fi

if [[ -d "task-template" ]]
then
    echo "task-template exists on your filesystem."
    exit 1
fi

echo "Cloning template repo..."

git clone git@gitlab.com:stacshack-2022/template-task.git # clone template

echo "Moving into 'task-template'..."
cd task-template
git submodule init # initialise template class
git submodule update --force --remote # update template class

echo "Pushing to new namespace... ($NAMESPACE)"
git remote remove origin # remove template report destination
git remote add origin git@gitlab.com:stacshack-2022/tasks/"$NAMESPACE".git # add new destination
git push --set-upstream origin master
git push -u origin --all # push everything
git push -u origin --tags
